package com.javastart.less_7_interfaces.abstractmethod;

import com.javastart.less_7_interfaces.abstractmethod.Creators.Creator;
import com.javastart.less_7_interfaces.abstractmethod.Creators.CreatorArcher;
import com.javastart.less_7_interfaces.abstractmethod.Creators.CreatorMag;
import com.javastart.less_7_interfaces.abstractmethod.Creators.CreatorWarrior;
import com.javastart.less_7_interfaces.abstractmethod.Entities.Human;

/**
 * В данном пакете нужно будет реализовать абстрактный метод.
 * Главным интерфейсом будет персонаж, то есть Human, у него будут абстрактные методы
 * say() и attack()
 *
 * так же будут классы, Mag, Archer, Warrior которые будут реализовывать интерфейс Human
 * метод say() и attack() нужно реализовать по такому примеру: Mag говорит что он маг и атакует с помощью магии посоха
 *
 * Далее нужно будет реализовать класс FactoryMethod и в нем написать метод, который будет возвращать тип Human
 * но создаст объект конкретного класса Mag, Warrior или Archer
 *
 * например так: public static Human factoryMethod(Class creationClass)
 * или другим любым спосбом (нужно придумать еще способ, значит рядом написать еще один фабричный метод
 * и определить логику, по которой будет возвращаться лучник, воин или маг)
 *
 * у созданных объектов вызвать все методы и посмотреть на них в консоле
 *
 * в main() небольшой пример работы с Class
 */

public class Main {

    public static void main(String[] args) {

        Creator creatorArcher = new CreatorArcher();
        Creator creatorMag = new CreatorMag();
        Creator creatorWarrior = new CreatorWarrior();

        Human mag = creatorMag.create();
        Human archer = creatorArcher.create();
        Human warrior = creatorWarrior.create();

        mag.attack();
        archer.say();
        warrior.attack();
    }

}
