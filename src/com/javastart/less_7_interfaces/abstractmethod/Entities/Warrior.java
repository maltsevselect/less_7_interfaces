package com.javastart.less_7_interfaces.abstractmethod.Entities;

public class Warrior extends Human {
    @Override
    public void say() {
        System.out.println("Я воин");
    }

    @Override
    public void attack() {
        System.out.println("Воин Атакует");
    }
}
