package com.javastart.less_7_interfaces.abstractmethod.Entities;

public class Mag extends Human {
    @Override
    public void say() {
        System.out.println("Я маг");
    }

    @Override
    public void attack() {
        System.out.println("Маг Атакует");
    }
}
