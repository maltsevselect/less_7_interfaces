package com.javastart.less_7_interfaces.abstractmethod.Entities;

public class Archer extends Human {
    @Override
    public void say() {
        System.out.println("Я лучник");
    }

    @Override
    public void attack() {
        System.out.println("Лучник Атакует");
    }
}
