package com.javastart.less_7_interfaces.abstractmethod.Entities;

public abstract class Human {

    public abstract void say();

    public abstract void attack();

}
