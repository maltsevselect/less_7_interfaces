package com.javastart.less_7_interfaces.abstractmethod.Creators;

import com.javastart.less_7_interfaces.abstractmethod.Entities.Human;
import com.javastart.less_7_interfaces.abstractmethod.Entities.Mag;

public class CreatorMag extends Creator{


    @Override
    public Human create() {
        return new Mag();
    }
}
