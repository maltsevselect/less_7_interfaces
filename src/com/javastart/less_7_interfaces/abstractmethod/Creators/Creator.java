package com.javastart.less_7_interfaces.abstractmethod.Creators;

import com.javastart.less_7_interfaces.abstractmethod.Entities.Human;

public abstract class Creator {

    public abstract Human create();
}
