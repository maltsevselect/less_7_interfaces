package com.javastart.less_7_interfaces.abstractmethod.Creators;

import com.javastart.less_7_interfaces.abstractmethod.Entities.Archer;
import com.javastart.less_7_interfaces.abstractmethod.Entities.Human;

public class CreatorArcher extends Creator{


    @Override
    public Human create() {
        return new Archer();
    }
}
