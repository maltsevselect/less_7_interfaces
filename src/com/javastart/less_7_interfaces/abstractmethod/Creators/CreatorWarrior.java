package com.javastart.less_7_interfaces.abstractmethod.Creators;

import com.javastart.less_7_interfaces.abstractmethod.Entities.Human;
import com.javastart.less_7_interfaces.abstractmethod.Entities.Warrior;

public class CreatorWarrior extends Creator{


    @Override
    public Human create() {
        return new Warrior();
    }
}
