package com.javastart.less_7_interfaces.abstractfabric;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.*;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Modern.ModernFurnitureFabric;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Victorian.VictorianFurnitureFabric;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Wood.WoodFurnitureFabric;

/**
 * В данном пакете нужно написать абстрактную фабрику мебели.
 * Этого можно достичь с помощью полифморфизма.
 *
 * Советую немного погуглить и почитать про фабрику и фабричный метод, это
 * достаточно популярные паттерны проектирования, неплохие и понятные материалы по ним
 * тут https://refactoring.guru/ru/design-patterns/abstract-factory
 *
 * В чем будет заключаться фабрика, вам нужно будет создать освновной интерфейс (или абстрактный класс): FurnitureFabric
 * у которого будут методы: createSofa, createChair, createBookshelf
 *
 * Далее будет три интерфейса: Sofa, Chair, Bookshelf
 *
 * У интерфейса Sofa будет метод goToSleep(), у Chair будет set(), у Bookshelf будет putBook() и getBook()
 *
 * Далее нужно будет создать отдельные классы, реализующие все три интерфейса, например WoodSofa implements Sofa и тд.
 *
 * Так же нужно реализовать интерфейс FurnitureFabric, например классом WoodFurnitureFabric и реализовать методы получения
 * дивана, стула, книжной полки
 *
 * Нужно создать не только WoodFurnitureFabric а еще несколько по тому же принципу
 *
 * Логика создания мебели лежит только в классах реализующих интерфейсы
 *
 * Нужно логировать каждое действие, объекты мебели нужно получить в методе main
 *
 * подсматривать можно сюда http://www.javenue.info/post/27
 *
 */

public class Main {

    public static void main(String[] args) {
        FurnitureFabric modernFurnitureFabric = new ModernFurnitureFabric();
        FurnitureFabric woodFurnitureFabric = new WoodFurnitureFabric();
        FurnitureFabric victorianFurnitureFabric = new VictorianFurnitureFabric();

        Bookshelf modernBookshelf = modernFurnitureFabric.createBookshelf();
        Chair modernChair = modernFurnitureFabric.createChair();
        Sofa modernSofa = modernFurnitureFabric.createSofa();

        Bookshelf victorianBookshelf = victorianFurnitureFabric.createBookshelf();
        Chair victorianChair = victorianFurnitureFabric.createChair();
        Sofa victorianSofa = victorianFurnitureFabric.createSofa();

        Bookshelf woodBookshelf = woodFurnitureFabric.createBookshelf();
        Chair woodChair = woodFurnitureFabric.createChair();
        Sofa woodSofa = woodFurnitureFabric.createSofa();

        modernBookshelf.getBook();
        modernChair.set();

        victorianBookshelf.putBook();
        victorianSofa.goToSleep();

        woodBookshelf.putBook();
        woodChair.set();



    }
}
