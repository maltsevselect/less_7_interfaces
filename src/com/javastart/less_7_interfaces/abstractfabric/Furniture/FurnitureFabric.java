package com.javastart.less_7_interfaces.abstractfabric.Furniture;

public abstract class FurnitureFabric {

    public abstract Sofa createSofa();

    public abstract Chair createChair();

    public abstract Bookshelf createBookshelf();
}
