package com.javastart.less_7_interfaces.abstractfabric.Furniture;

public interface Bookshelf {

    public void putBook();

    public void getBook();
}
