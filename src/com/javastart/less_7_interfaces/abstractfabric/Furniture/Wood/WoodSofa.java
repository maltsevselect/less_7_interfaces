package com.javastart.less_7_interfaces.abstractfabric.Furniture.Wood;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Sofa;

public class WoodSofa implements Sofa {


    @Override
    public void goToSleep() {
        System.out.println("Пошли спать на деревянной софе");
    }

}
