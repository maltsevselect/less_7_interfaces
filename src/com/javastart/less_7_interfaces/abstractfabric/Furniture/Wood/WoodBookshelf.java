package com.javastart.less_7_interfaces.abstractfabric.Furniture.Wood;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Bookshelf;

public class WoodBookshelf implements Bookshelf {
    @Override
    public void putBook() {
        System.out.println("Книга поставлена на деревянный стелаж");
    }

    @Override
    public void getBook() {
        System.out.println("Книга взята с деревянного стелажа");
    }
}
