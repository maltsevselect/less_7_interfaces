package com.javastart.less_7_interfaces.abstractfabric.Furniture.Wood;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Bookshelf;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Chair;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.FurnitureFabric;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Sofa;

public class WoodFurnitureFabric extends FurnitureFabric {
    @Override
    public Sofa createSofa() {
        return new WoodSofa();
    }

    @Override
    public Chair createChair() {
        return new WoodChair();
    }

    @Override
    public Bookshelf createBookshelf() {
        return new WoodBookshelf();
    }
}
