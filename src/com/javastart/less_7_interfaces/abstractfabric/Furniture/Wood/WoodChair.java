package com.javastart.less_7_interfaces.abstractfabric.Furniture.Wood;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Chair;

public class WoodChair implements Chair {
    @Override
    public void set() {
        System.out.println("Поставлен деревянный стул");
    }
}
