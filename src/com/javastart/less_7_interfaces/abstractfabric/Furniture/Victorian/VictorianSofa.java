package com.javastart.less_7_interfaces.abstractfabric.Furniture.Victorian;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Sofa;

public class VictorianSofa implements Sofa {


    @Override
    public void goToSleep() {
        System.out.println("Пошли спать на викторианской софе");
    }
}
