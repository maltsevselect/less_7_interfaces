package com.javastart.less_7_interfaces.abstractfabric.Furniture.Victorian;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Bookshelf;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Chair;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.FurnitureFabric;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Sofa;

public class VictorianFurnitureFabric extends FurnitureFabric {
    @Override
    public Sofa createSofa() {
        return new VictorianSofa();
    }

    @Override
    public Chair createChair() {
        return new VictorianChair();
    }

    @Override
    public Bookshelf createBookshelf() {
        return new VictorianBookshelf();
    }
}
