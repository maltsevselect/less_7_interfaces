package com.javastart.less_7_interfaces.abstractfabric.Furniture.Victorian;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Bookshelf;

public class VictorianBookshelf implements Bookshelf {
    @Override
    public void putBook() {
        System.out.println("Книга поставлена на викторианский стелаж");
    }

    @Override
    public void getBook() {
        System.out.println("Книга взята с викторианского стелажа");
    }
}
