package com.javastart.less_7_interfaces.abstractfabric.Furniture.Victorian;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Chair;

public class VictorianChair implements Chair {
    @Override
    public void set() {
        System.out.println("Поставлен викторианский стул");
    }
}
