package com.javastart.less_7_interfaces.abstractfabric.Furniture.Modern;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Bookshelf;

public class ModernBookshelf implements Bookshelf {
    @Override
    public void putBook() {
        System.out.println("Книга поставлена на модерн стелаж");
    }

    @Override
    public void getBook() {
        System.out.println("Книга взята с модерн стелажа");
    }
}
