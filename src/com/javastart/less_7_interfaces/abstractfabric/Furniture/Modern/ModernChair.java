package com.javastart.less_7_interfaces.abstractfabric.Furniture.Modern;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Chair;

public class ModernChair implements Chair {
    @Override
    public void set() {
        System.out.println("Поставлен модерн стул");
    }
}
