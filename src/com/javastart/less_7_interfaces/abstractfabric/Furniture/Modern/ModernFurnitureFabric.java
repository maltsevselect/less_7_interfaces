package com.javastart.less_7_interfaces.abstractfabric.Furniture.Modern;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Bookshelf;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Chair;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.FurnitureFabric;
import com.javastart.less_7_interfaces.abstractfabric.Furniture.Sofa;

public class ModernFurnitureFabric extends FurnitureFabric {
    @Override
    public Sofa createSofa() {
        return new ModernSofa();
    }

    @Override
    public Chair createChair() {
        return new ModernChair();
    }

    @Override
    public Bookshelf createBookshelf() {
        return new ModernBookshelf();
    }
}
