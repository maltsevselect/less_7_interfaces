package com.javastart.less_7_interfaces.abstractfabric.Furniture.Modern;

import com.javastart.less_7_interfaces.abstractfabric.Furniture.Sofa;

public class ModernSofa implements Sofa {


    @Override
    public void goToSleep() {
        System.out.println("Пошли спать на модерн софе");
    }
}
